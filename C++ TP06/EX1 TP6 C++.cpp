#include <iostream>
#include <cmath>

using namespace std;
class Point{
	protected:
	    int x,y;
	public:
		Point(int a=0,int b=0){ x=a;y=b; }
		int getX(){ return x;  }
		int getY(){ return y;  }
		void affiche(){  cout<<"\nX="<<x<<"\tY="<<y;  }
};
class PointPolairePub:public Point{
	float r;
	public:
		PointPolairePub(int a,int b):Point(a,b){ r=this->rho(); }
		float rho(){ r=sqrt(pow(x,2)+pow(y,2));  }
		void affiche(){ Point::affiche(); cout<<"\tR="<<r;  }
		
};
class PointPolairePriv:private Point{
	float r;
	public:
		int getX(){ return Point::getX();  }
		int getY(){ return Point::getY();  }
		PointPolairePriv(int a,int b):Point(a,b){ r=this->rho(); }
		float rho(){ r=sqrt(pow(x,2)+pow(y,2)); return r;   }
		void affiche(){ cout<<"\nX="<<PointPolairePriv::getX()<<"\tY="<<PointPolairePriv::getY()<<"\tR="<<r; }
		

};
int main(int argc, char** argv) {
	Point A(2,4);
	A.affiche();
	PointPolairePub B(4,7);
	B.affiche();
	PointPolairePriv C(3,2);
	C.affiche();
	
	return 0;
}
