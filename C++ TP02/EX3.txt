#include <iostream>
#include <cmath>
using namespace std;
class vecteur3d{
	float x,y,z;
	public:
	 vecteur3d(float c1=0.0,float c2=0.0,float c3=0.0){   x=c1; y=c2; z=c3;   }
	 vecteur3d normeC(vecteur3d );
	 vecteur3d& normeC_Ref(vecteur3d &);
	  vecteur3d*normeC_Add(vecteur3d * );
	 void affiche();
	 float Norme();
	 int Comparer(vecteur3d );
	 
};
int  vecteur3d::Comparer(vecteur3d B){
	if( x==B.x && y==B.y && z==B.z){  return 1;   }
	else {  return 0;  }
}
float vecteur3d::Norme(){
	float Norm;
	Norm=sqrt(pow(x,2)+pow(y,2)+pow(z,2));
	return Norm;
}
void vecteur3d::affiche(){
	cout <<"X=: "<<x<<"\t"<<"Y=: "<<y<<"\t"<<"Z=:  "<<z<<"\n";
}
vecteur3d vecteur3d::normeC(vecteur3d b ){
	if(b.Norme()>=this->Norme()) return b;
	else return *(this);
}
vecteur3d& vecteur3d::normeC_Ref(vecteur3d &d){
    if( this->Norme()>=d.Norme()) { return d;}
    else  return *(this);
}
vecteur3d *vecteur3d::normeC_Add(vecteur3d *b ){
	if( this->Norme()>=b->Norme()) { return b;}
    else  return this;
}
int main(int argc, char** argv) {
	float a,b,c;
	int Com;
	cout<<"Entree Les Valeurs De A : \n";
	cout<<"Valeur De X :";
	cin>>a;
	cout<<"Valeur De Y :";
	cin>>b;
	cout<<"Valeur De Z :";
	cin>>c;
	cout<<"Les Valeur De A : \n";
	vecteur3d A(a,b,c);
	A.affiche();
	cout<<"Entree Les Valeurs De B : \n";
	cout<<"Valeur De X :";
	cin>>a;
	cout<<"Valeur De Y :";
	cin>>b;
	cout<<"Valeur De Z :";
	cin>>c;
	cout<<"Les Valeur De A : \n";
	vecteur3d B(a,b,c);
	B.affiche();
	vecteur3d C=A.normeC(B);
	if(C.Comparer(A)==1){  cout<<"A est a La Grand Norme"; }
	else cout<<"B est a La Grand Norme";

	return 0;
}